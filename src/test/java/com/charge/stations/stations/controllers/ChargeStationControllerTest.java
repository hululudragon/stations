package com.charge.stations.stations.controllers;

import com.charge.stations.stations.models.ChargeStation;
import com.charge.stations.stations.models.GeoLocation;
import com.charge.stations.stations.services.ChargeStationServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

import static com.charge.stations.stations.constants.ChargeStationConstants.API_PREFIX;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ChargeStationController.class)
public class ChargeStationControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private ChargeStationServiceImpl chargeStationService;

    @Test
    public void whenGETChargeStationsByLatitudeLongitudeAndDistanceFromPointThenReturnChargeStationsAnd200() throws Exception {

        double latitude = 18.180555;
        double longitude = -66.749961;

        List<ChargeStation> chargeStationsCloseBy = new ArrayList<>();


        GeoJsonPoint firstStationPoint = new GeoJsonPoint(-66.749961, 18.180555);
        GeoJsonPoint secondStationPoint = new GeoJsonPoint(-66.832041, 18.190607);
        GeoJsonPoint thirdStationPoint = new GeoJsonPoint(-66.712985, 18.263085);

        ChargeStation firstChargeStation = new ChargeStation("1", "00601", firstStationPoint);
        ChargeStation secondChargeStation = new ChargeStation("2", "00631", secondStationPoint);
        ChargeStation thirdChargeStation = new ChargeStation("3", "00641", thirdStationPoint);

        chargeStationsCloseBy.add(firstChargeStation);
        chargeStationsCloseBy.add(secondChargeStation);
        chargeStationsCloseBy.add(thirdChargeStation);


        Mockito.doReturn(chargeStationsCloseBy).when(chargeStationService).getCloseChargeStationsByGivenDistance(latitude, longitude, 10);


        mockMvc.perform(MockMvcRequestBuilders.get(API_PREFIX + "/charge-stations?latitude=18.180555&longitude=-66.749961&distance=10")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(header().string(HttpHeaders.CONTENT_TYPE, "application/json"))
                .andExpect(MockMvcResultMatchers.content().string(objectMapper.writeValueAsString(chargeStationsCloseBy)))
                .andExpect(status().isOk());
    }

    @Test
    public void whenGETRequestForGetChargeStationsByZipCodeThenReturnChargeStations() throws Exception {

        List<ChargeStation> chargeStationsCloseBy = new ArrayList<>();

        GeoJsonPoint thirdStationPoint = new GeoJsonPoint(-66.712985, 18.263085);
        GeoJsonPoint fourthStationPoint = new GeoJsonPoint(-66.712984, 18.263083);

        ChargeStation thirdChargeStation = new ChargeStation("3", "00641", thirdStationPoint);
        ChargeStation fourthChargeStation = new ChargeStation("4", "00641", fourthStationPoint);

        chargeStationsCloseBy.add(thirdChargeStation);
        chargeStationsCloseBy.add(fourthChargeStation);

        Mockito.doReturn(chargeStationsCloseBy).when(chargeStationService).getChargeStationsByZipCode("00641");

        mockMvc.perform(MockMvcRequestBuilders.get(API_PREFIX + "/charge-stations/zip/{zip}", "00641")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(header().string(HttpHeaders.CONTENT_TYPE, "application/json"))
                .andExpect(MockMvcResultMatchers.content().string(objectMapper.writeValueAsString(chargeStationsCloseBy)))
                .andExpect(status().isOk());
    }

    @Test
    public void whenGETRequestChargeStationsByIdThenReturnChargeStationGivenId() throws Exception {

        List<ChargeStation> chargeStationsCloseBy = new ArrayList<>();

        GeoJsonPoint firstStationPoint = new GeoJsonPoint(-66.749961, 18.180555);
        GeoJsonPoint secondStationPoint = new GeoJsonPoint(-66.832041, 18.190607);

        ChargeStation firstChargeStation = new ChargeStation("1", "00601", firstStationPoint);

        ChargeStation secondChargeStation = new ChargeStation("2", "00631", secondStationPoint);

        chargeStationsCloseBy.add(firstChargeStation);
        chargeStationsCloseBy.add(secondChargeStation);


        Mockito.doReturn(firstChargeStation).when(chargeStationService).getChargeStationById(firstChargeStation.getChargeStationId());

        mockMvc.perform(MockMvcRequestBuilders.get(API_PREFIX + "/charge-stations/id/{id}", "1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(header().string(HttpHeaders.CONTENT_TYPE, "application/json"))
                .andExpect(MockMvcResultMatchers.content().string(objectMapper.writeValueAsString(firstChargeStation)))
                .andExpect(status().isOk());
    }

    @Test
    public void whenPOSTListOfChargeStationsThenReturnChargeStationsAnd201() throws Exception {

        List<ChargeStation> savedChargeStations = new ArrayList<>();

        List<GeoLocation> chargeStationsLocations = new ArrayList<>();

        GeoLocation firstStationLocation = new GeoLocation("00601", -66.749961, 18.180555);
        GeoLocation secondStationLocation = new GeoLocation("00631", -66.832041, 18.190607);
        GeoLocation thirdStationLocation = new GeoLocation("00641", -66.712985, 18.263085);
        GeoLocation fourthStationLocation = new GeoLocation("00641", -66.712984, 18.263083);

        chargeStationsLocations.add(firstStationLocation);
        chargeStationsLocations.add(secondStationLocation);
        chargeStationsLocations.add(thirdStationLocation);
        chargeStationsLocations.add(fourthStationLocation);

        GeoJsonPoint firstStationPoint = new GeoJsonPoint(-66.749961, 18.180555);
        GeoJsonPoint secondStationPoint = new GeoJsonPoint(-66.832041, 18.190607);
        GeoJsonPoint thirdStationPoint = new GeoJsonPoint(-66.712985, 18.263085);
        GeoJsonPoint fourthStationPoint = new GeoJsonPoint(-66.712984, 18.263083);

        ChargeStation firstChargeStation = new ChargeStation("1", "00601", firstStationPoint);
        ChargeStation secondChargeStation = new ChargeStation("2", "00631", secondStationPoint);
        ChargeStation thirdChargeStation = new ChargeStation("3", "00641", thirdStationPoint);
        ChargeStation fourthChargeStation = new ChargeStation("4", "00641", fourthStationPoint);

        savedChargeStations.add(firstChargeStation);
        savedChargeStations.add(secondChargeStation);
        savedChargeStations.add(thirdChargeStation);
        savedChargeStations.add(fourthChargeStation);

        Mockito.doReturn(savedChargeStations).when(chargeStationService).addChargeStations(chargeStationsLocations);

        mockMvc.perform(MockMvcRequestBuilders.post(API_PREFIX + "/charge-stations")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(chargeStationsLocations)))
                .andExpect(header().string(HttpHeaders.CONTENT_TYPE, "application/json"))
                .andExpect(MockMvcResultMatchers.content().string(objectMapper.writeValueAsString(savedChargeStations)))
                .andExpect(status().isCreated());
    }

}