package com.charge.stations.stations.services;

import com.charge.stations.stations.errors.ChargeStationNotFoundException;
import com.charge.stations.stations.models.ChargeStation;
import com.charge.stations.stations.models.GeoLocation;
import com.charge.stations.stations.repositories.ChargeStationRepository;
import com.charge.stations.stations.utils.GenerateIncrementalIdImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.Metrics;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.test.context.ContextConfiguration;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.Silent.class)
@ContextConfiguration(classes = ChargeStationServiceImpl.class)
public class ChargeStationServiceImplTest {

    @InjectMocks
    private ChargeStationServiceImpl chargeStationServiceImpl;

    @Mock
    private ChargeStationRepository chargeStationRepository;

    @Mock
    private GenerateIncrementalIdImpl nextSequenceService;

    @Test
    public void whenChargeStationRequestedByIdThenReturnChargeStation() {

        GeoJsonPoint firstStationPoint = new GeoJsonPoint(-66.749961, 18.180555);

        ChargeStation firstChargeStation = new ChargeStation("1", "00601", firstStationPoint);

        given(chargeStationRepository.findByChargeStationId("1")).willReturn(firstChargeStation);

        ChargeStation chargeStation = chargeStationServiceImpl.getChargeStationById("1");

        verify(chargeStationRepository).findByChargeStationId("1");

        assertEquals(firstChargeStation.getChargeStationId(), chargeStation.getChargeStationId());
        assertEquals(firstChargeStation.getZipCode(), chargeStation.getZipCode());
        assertEquals(firstChargeStation.getLocation(), chargeStation.getLocation());
    }

    @Test
    public void whenChargeStationRequestedByZipCodeThenReturnChargeStation() {
        String zipCode = "00601";

        List<ChargeStation> chargeStationsCloseBy = new ArrayList<>();
        GeoJsonPoint thirdStationPoint = new GeoJsonPoint(-66.712985, 18.263085);
        GeoJsonPoint fourthStationPoint = new GeoJsonPoint(-66.712984, 18.263083);

        ChargeStation thirdChargeStation = new ChargeStation("3", "00641", thirdStationPoint);
        ChargeStation fourthChargeStation = new ChargeStation("4", "00641", fourthStationPoint);

        chargeStationsCloseBy.add(thirdChargeStation);
        chargeStationsCloseBy.add(fourthChargeStation);

        given(chargeStationRepository.findByZipCode(zipCode)).willReturn(chargeStationsCloseBy);

        List<ChargeStation> chargeStation = chargeStationServiceImpl.getChargeStationsByZipCode(zipCode);

        verify(chargeStationRepository, times(2)).findByZipCode(zipCode);

        assertEquals(thirdChargeStation, chargeStationsCloseBy.get(0));
        assertEquals(fourthChargeStation, chargeStation.get(1));
    }

    @Test
    public void whenGivenLatitudeAndLongitudeWithDistanceThenReturnChargeStationsWithinGivenDistance() {

        double latitudeOfGivenGeolocation = 18.180555;
        double longitudeOfGivenGeolocation = -66.749961;
        double distance = 10.0;
        List<ChargeStation> chargeStationsCloseBy = new ArrayList<>();

        GeoJsonPoint firstStationPoint = new GeoJsonPoint(-66.749961, 18.180555);
        GeoJsonPoint secondStationPoint = new GeoJsonPoint(-66.832041, 18.190607);
        GeoJsonPoint thirdStationPoint = new GeoJsonPoint(-66.712985, 18.263085);
        GeoJsonPoint fourthStationPoint = new GeoJsonPoint(-66.712984, 18.263083);


        ChargeStation firstChargeStation = new ChargeStation("1", "00601", firstStationPoint);
        ChargeStation secondChargeStation = new ChargeStation("2", "00631", secondStationPoint);
        ChargeStation thirdChargeStation = new ChargeStation("3", "00641", thirdStationPoint);
        ChargeStation fourthChargeStation = new ChargeStation("4", "00641", fourthStationPoint);

        chargeStationsCloseBy.add(firstChargeStation);
        chargeStationsCloseBy.add(secondChargeStation);
        chargeStationsCloseBy.add(thirdChargeStation);
        chargeStationsCloseBy.add(fourthChargeStation);


        Point point = new Point(longitudeOfGivenGeolocation, latitudeOfGivenGeolocation);
        Distance distanceWithKms = new Distance(distance, Metrics.KILOMETERS);

        given(chargeStationRepository.findByLocationNear(point, distanceWithKms)).willReturn(chargeStationsCloseBy);

        List<ChargeStation> chargeStations = chargeStationServiceImpl.getCloseChargeStationsByGivenDistance(latitudeOfGivenGeolocation, longitudeOfGivenGeolocation, distance);

        verify(chargeStationRepository).findByLocationNear(point, distanceWithKms);

        assertEquals(4, chargeStations.size());
        assertEquals(firstChargeStation, chargeStations.get(0));
        assertEquals(secondChargeStation, chargeStations.get(1));
        assertEquals(thirdChargeStation, chargeStations.get(2));
        assertEquals(fourthChargeStation, chargeStations.get(3));
    }

    @Test
    public void whenThereIsNoChargeStationGivenLatitudeAndLongitudeWithDistanceThenReturnChargeStationsNotFoundErrorWithinGivenDistance() {

        double latitudeOfGivenGeolocation = 18.180555;
        double longitudeOfGivenGeolocation = -66.749961;
        double distance = 10.0;
        List<ChargeStation> chargeStationsCloseBy = new ArrayList<>();

        Point point = new Point(longitudeOfGivenGeolocation, latitudeOfGivenGeolocation);
        Distance distanceWithKms = new Distance(distance, Metrics.KILOMETERS);

        given(chargeStationRepository.findByLocationNear(point, distanceWithKms)).willReturn(chargeStationsCloseBy);

        try {
            chargeStationServiceImpl.getCloseChargeStationsByGivenDistance(latitudeOfGivenGeolocation, longitudeOfGivenGeolocation, distance);
            fail();
        } catch (ChargeStationNotFoundException ex) {
            assertEquals("ChargeStationService charge station not found " + latitudeOfGivenGeolocation + " latitude and " +
                    "" + longitudeOfGivenGeolocation + " longitude in distance " + distance + " km not found", ex.getMessage());
        }
        verify(chargeStationRepository).findByLocationNear(point, distanceWithKms);
    }

    @Test
    public void whenGivenNonExistentChargeStationIdThenReturnChargeStationsNotFoundErrorForGivenId() {

        String chargeStationId = "1234";
        given(chargeStationRepository.findByChargeStationId(chargeStationId)).willReturn(null);

        try {
            chargeStationServiceImpl.getChargeStationById(chargeStationId);
            fail();
        } catch (ChargeStationNotFoundException ex) {
            assertEquals("ChargeStationService charge station not found with Id " + chargeStationId, ex.getMessage());
        }
        verify(chargeStationRepository).findByChargeStationId(chargeStationId);
    }

    @Test
    public void whenThereIsNoChargeStationGivenZipCodeThenReturnChargeStationsNotFoundErrorForGivenZipCode() {

        String zipCode = "35540";
        List<ChargeStation> chargeStationsCloseBy = new ArrayList<>();
        given(chargeStationRepository.findByZipCode(zipCode)).willReturn(chargeStationsCloseBy);

        try {
            chargeStationServiceImpl.getChargeStationsByZipCode(zipCode);
            fail();
        } catch (ChargeStationNotFoundException ex) {
            assertEquals("ChargeStationService charge station not found  in zip code " + zipCode, ex.getMessage());
        }
        verify(chargeStationRepository).findByZipCode(zipCode);
    }

    @Test
    public void whenGivenListOfChargeStationsWithLatitudeLongitudeAndZipCodeThenSaveAllAndReturnAll() {

        List<ChargeStation> chargeStationsToSave = new ArrayList<>();

        List<GeoLocation> chargeStationsLocations = new ArrayList<>();

        GeoLocation firstStationLocation = new GeoLocation("00601", -66.749961, 18.180555);
        GeoLocation secondStationLocation = new GeoLocation("00631", -66.832041, 18.190607);
        GeoLocation thirdStationLocation = new GeoLocation("00641", -66.712985, 18.263085);
        GeoLocation fourthStationLocation = new GeoLocation("00641", -66.712984, 18.263083);

        chargeStationsLocations.add(firstStationLocation);
        chargeStationsLocations.add(secondStationLocation);
        chargeStationsLocations.add(thirdStationLocation);
        chargeStationsLocations.add(fourthStationLocation);

        GeoJsonPoint firstStationPoint = new GeoJsonPoint(-66.749961, 18.180555);
        GeoJsonPoint secondStationPoint = new GeoJsonPoint(-66.832041, 18.190607);
        GeoJsonPoint thirdStationPoint = new GeoJsonPoint(-66.712985, 18.263085);
        GeoJsonPoint fourthStationPoint = new GeoJsonPoint(-66.712984, 18.263083);

        List<GeoJsonPoint> chargeStationsPoint = new ArrayList<>();
        chargeStationsPoint.add(firstStationPoint);
        chargeStationsPoint.add(secondStationPoint);
        chargeStationsPoint.add(thirdStationPoint);
        chargeStationsPoint.add(fourthStationPoint);

        List<String> customSequences = new ArrayList<>();
        customSequences.add("1");
        customSequences.add("2");
        customSequences.add("3");
        customSequences.add("4");

        for (int i = 0; i < chargeStationsLocations.size(); i++) {
            given(nextSequenceService.generateSequence("customSequences")).willReturn(customSequences.get(i));
            chargeStationsToSave.add(new ChargeStation(customSequences.get(i), chargeStationsLocations.get(i).getZipCode(), chargeStationsPoint.get(i)));
        }

        given(chargeStationRepository.saveAll(chargeStationsToSave)).willReturn(chargeStationsToSave);
        when(this.chargeStationServiceImpl.addChargeStations(chargeStationsLocations)).thenReturn(chargeStationsToSave);

        assertEquals(4, chargeStationsToSave.size());
    }


}