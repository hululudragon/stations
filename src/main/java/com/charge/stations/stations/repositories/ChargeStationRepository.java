package com.charge.stations.stations.repositories;

import com.charge.stations.stations.models.ChargeStation;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface ChargeStationRepository extends MongoRepository<ChargeStation, String> {

    List<ChargeStation> findByLocationNear(Point location, Distance distance);

    ChargeStation findByChargeStationId(String chargeStationId);

    List<ChargeStation> findByZipCode(String zipCode);

}