package com.charge.stations.stations.errors;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(ChargeStationNotFoundException.class)
    protected ResponseEntity<ApiError> handleEntityNotFound(ChargeStationNotFoundException ex) {
        ApiError apiError = new ApiError(NOT_FOUND, "Charge Station Not Found", ex);
        return new ResponseEntity<>(apiError, apiError.getStatus());
    }

}
