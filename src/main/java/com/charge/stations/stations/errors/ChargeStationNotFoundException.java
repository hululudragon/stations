package com.charge.stations.stations.errors;

import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ChargeStationNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public ChargeStationNotFoundException(Class clazz, String searchParamsMap) {
        super(ChargeStationNotFoundException.generateMessage(clazz.getSimpleName(), searchParamsMap));
    }

    private static String generateMessage(String entity, String searchParams) {
        return StringUtils.capitalize(entity) + " charge station not found " + searchParams;
    }
}
