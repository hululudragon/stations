package com.charge.stations.stations.constants;

public final class ChargeStationConstants {

    public static final String API_PREFIX = "/api/v1";

    private ChargeStationConstants() {
    }
}
