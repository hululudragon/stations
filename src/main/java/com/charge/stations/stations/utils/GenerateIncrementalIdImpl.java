package com.charge.stations.stations.utils;

import com.charge.stations.stations.models.UserDefinedId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.util.Objects;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

@Service
public class GenerateIncrementalIdImpl implements GenerateIncrementalId {
    MongoOperations mongoOperations;

    @Autowired
    public GenerateIncrementalIdImpl(MongoOperations mongoOperations) {
        this.mongoOperations = mongoOperations;
    }

    public String generateSequence(String seqName) {
        UserDefinedId counter =
                mongoOperations.findAndModify(
                        query(where("_id").is(seqName)),
                        new Update().inc("seq", 1),
                        FindAndModifyOptions.options().returnNew(true).upsert(true),
                        UserDefinedId.class
                );
        return !Objects.isNull(counter) ? counter.getSeq() : "1";
    }
}
