package com.charge.stations.stations.utils;

public interface GenerateIncrementalId {
     String generateSequence(String seqName);
}
