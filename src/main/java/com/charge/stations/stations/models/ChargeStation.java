package com.charge.stations.stations.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexType;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Objects;

@Document(collection = "chargeStations")

public class ChargeStation {
    @Id
    private String chargeStationId;
    private String zipCode;

    @GeoSpatialIndexed(type = GeoSpatialIndexType.GEO_2DSPHERE)
    private GeoJsonPoint location;

    public ChargeStation() {
    }

    public ChargeStation(final String chargeStationId, final String zipCode, final GeoJsonPoint location) {
        this.chargeStationId = chargeStationId;
        this.zipCode = zipCode;
        this.location = location;
    }

    public String getChargeStationId() {
        return this.chargeStationId;
    }

    public String getZipCode() {
        return this.zipCode;
    }

    public GeoJsonPoint getLocation() {
        return this.location;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || this.getClass() != o.getClass()) return false;
        final ChargeStation that = (ChargeStation) o;
        return Objects.equals(this.getChargeStationId(), that.getChargeStationId())
                &&
                Objects.equals(this.getZipCode(),
                        that.getZipCode());
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.getChargeStationId(), this.getZipCode());
    }

}