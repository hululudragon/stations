package com.charge.stations.stations.models;

import java.io.Serializable;
import java.util.Objects;

public class GeoLocation implements Serializable {

    private double longitude;
    private double latitude;
    private String zipCode;

    public GeoLocation(String zipCode, double latitude, double longitude) {
        this.longitude = longitude;
        this.latitude = latitude;
        this.zipCode = zipCode;
    }

    public String getZipCode() {
        return zipCode;
    }

    public double getLongitude() {
        return this.longitude;
    }

    public double getLatitude() {
        return this.latitude;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || this.getClass() != o.getClass()) return false;
        final GeoLocation that = (GeoLocation) o;
        return Objects.equals(this.getLongitude(), that.getLongitude()) &&
                Objects.equals(this.getLatitude(), that.getLatitude());
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.getLongitude(), this.getLatitude());
    }
}
