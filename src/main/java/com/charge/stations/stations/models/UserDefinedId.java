package com.charge.stations.stations.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "customSequences")
public class UserDefinedId {
    @Id
    private String id;
    private String seq;

    public String getSeq() {
        return seq;
    }

}
