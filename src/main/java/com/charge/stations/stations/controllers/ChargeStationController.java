package com.charge.stations.stations.controllers;

import com.charge.stations.stations.models.ChargeStation;
import com.charge.stations.stations.models.GeoLocation;
import com.charge.stations.stations.services.ChargeStationServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.charge.stations.stations.constants.ChargeStationConstants.API_PREFIX;

@RestController
@RequestMapping(API_PREFIX + "/charge-stations")
public class ChargeStationController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ChargeStationController.class);

    private final ChargeStationServiceImpl chargeStationServiceImpl;

    @Autowired
    public ChargeStationController(ChargeStationServiceImpl chargeStationServiceImpl) {
        this.chargeStationServiceImpl = chargeStationServiceImpl;
    }

    @GetMapping()
    public final List<ChargeStation> getChargeStationsForGivenPointByDistance(
            @RequestParam("latitude") double latitude,
            @RequestParam("longitude") double longitude,
            @RequestParam("distance") double distance) {

        return chargeStationServiceImpl.getCloseChargeStationsByGivenDistance(latitude, longitude, distance);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public final List<ChargeStation> addChargeStations(@RequestBody List<GeoLocation> entries) {
        return chargeStationServiceImpl.addChargeStations(entries);
    }

    @GetMapping("/id/{id}")
    public ChargeStation getChargeStationById(@PathVariable String id) {
        LOGGER.info("getAllChargeStations controller  initialized");
        return chargeStationServiceImpl.getChargeStationById(id);
    }

    @GetMapping("/zip/{zip}")
    public List<ChargeStation> getChargeStationsByZipCode(@PathVariable String zip) {
        LOGGER.info("getChargeStationsByZipCode controller initialized");
        return chargeStationServiceImpl.getChargeStationsByZipCode(zip);
    }

}
