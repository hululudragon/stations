package com.charge.stations.stations.services;

import com.charge.stations.stations.models.ChargeStation;
import com.charge.stations.stations.models.GeoLocation;

import java.util.List;

public interface ChargeStationService {
    ChargeStation getChargeStationById(String chargeStationId);

    List<ChargeStation> getChargeStationsByZipCode(String zipCode);

    List<ChargeStation> addChargeStations(List<GeoLocation> entries);

    List<ChargeStation> getCloseChargeStationsByGivenDistance(double latitude, double longitude, double distance);
}
