package com.charge.stations.stations.services;

import com.charge.stations.stations.errors.ChargeStationNotFoundException;
import com.charge.stations.stations.models.ChargeStation;
import com.charge.stations.stations.models.GeoLocation;
import com.charge.stations.stations.repositories.ChargeStationRepository;
import com.charge.stations.stations.utils.GenerateIncrementalIdImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.Metrics;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class ChargeStationServiceImpl implements ChargeStationService {

    private final Logger logger = LoggerFactory.getLogger(ChargeStationServiceImpl.class);

    private final ChargeStationRepository chargeStationRepository;
    private final GenerateIncrementalIdImpl generateIncrementalIdImpl;

    @Autowired
    ChargeStationServiceImpl(ChargeStationRepository chargeStationRepository, GenerateIncrementalIdImpl generateIncrementalIdImpl) {
        this.chargeStationRepository = chargeStationRepository;
        this.generateIncrementalIdImpl = generateIncrementalIdImpl;
    }

    @Override
    public ChargeStation getChargeStationById(String chargeStationId) {
        logger.info("getChargeStationById service initialized");
        ChargeStation chargeStationInGivenZipCode = chargeStationRepository.findByChargeStationId(chargeStationId);

        if (chargeStationInGivenZipCode == null) {
            throw new ChargeStationNotFoundException(ChargeStationService.class, "with Id " + chargeStationId);
        }
        return chargeStationInGivenZipCode;
    }

    @Override
    public List<ChargeStation> addChargeStations(List<GeoLocation> entries) {
        logger.info("addChargeStations service initialized");

        List<ChargeStation> entities = new ArrayList<>();
        for (GeoLocation location : entries) {
            final GeoJsonPoint locationPoint = new GeoJsonPoint(
                    location.getLongitude(),
                    location.getLatitude()
            );

            String zipCode = location.getZipCode();
            String customSequences = generateIncrementalIdImpl.generateSequence("customSequences");
            entities.add(new ChargeStation(customSequences, zipCode, locationPoint));
        }

        return chargeStationRepository.saveAll(entities);
    }

    @Override
    public List<ChargeStation> getChargeStationsByZipCode(String zipCode) {
        logger.info("getChargeStationsByZipCode service initialized");
        List<ChargeStation> chargeStationInGivenZipCode = chargeStationRepository.findByZipCode(zipCode);
        if (chargeStationInGivenZipCode.isEmpty()) {
            throw new ChargeStationNotFoundException(ChargeStationService.class, " in zip code " + zipCode);
        }
        return chargeStationRepository.findByZipCode(zipCode);
    }

    @Override
    public List<ChargeStation> getCloseChargeStationsByGivenDistance(double latitude, double longitude, double distance) {
        logger.info("getCloseChargeStationsByGivenDistance service initialized");
        Point currentPoint = new Point(longitude, latitude);
        Distance distanceToLookup = new Distance(distance, Metrics.KILOMETERS);

        List<ChargeStation> chargeStations = chargeStationRepository.findByLocationNear(currentPoint, distanceToLookup);
        if (chargeStations.isEmpty()) {
            throw new ChargeStationNotFoundException(ChargeStationService.class, latitude + " latitude and " +
                    "" + longitude + " longitude in distance " + distance + " km not found");
        }
        return chargeStations;
    }

}
