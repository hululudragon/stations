### Pre-requisites   
    Java 8
    Git 
    maven
    Docker
    
### Build
    mvn clean install
    mvn clean test
	mvn clean package
    test coverage report is generated in here: target/site/jacoco/index.html

### Run local without docker
	java -jar target/stations-0.0.1-SNAPSHOT.jar server application.yml
	
#### Run in Docker	  
	docker build -t stations/charge . 
	docker-compose up

### Open browser pointing at
    Docker Default ip is 192.168.99.100. It is better to double check
	http://192.168.99.100:8080	
## How to test application with interface?
#### Postman collections for testing purpose can be found here:
    https://gitlab.com/suatt/charge-stations-postman-collections
####  Postman app could be found here: 
    https://www.postman.com/downloads/
## Endpoints:
#### getChargingStationsByDistance:		
	GET URL: {{chargeStationsBaseUrl}}/api/v1/charge-stations?latitude={latitude}5&longitude={longitude}&distance={distance}
	GET SAMPLE URL: {{chargeStationsBaseUrl}}/api/v1/charge-stations?latitude=18.180555&longitude=-66.74996&distance=10.0
	sample respons:
	   [
           {
               "chargeStationId": "1760",
               "zipCode": "00601",
               "location": {
                   "x": -66.749951,
                   "y": 18.180555,
                   "type": "Point",
                   "coordinates": [
                       -66.749961,
                       18.180555
                   ]
               }
           },
           {
               "chargeStationId": "1790",
               "zipCode": "00601",
               "location": {
                   "x": -66.749931,
                   "y": 18.180555,
                   "type": "Point",
                   "coordinates": [
                       -66.749961,
                       18.180555
                   ]
               }
           },
           {
               "chargeStationId": "1820",
               "zipCode": "00601",
               "location": {
                   "x": -66.749941,
                   "y": 18.180555,
                   "type": "Point",
                   "coordinates": [
                       -66.749961,
                       18.180555
                   ]
               }
           }
     }      
#### getChargeStationsByZipCode:	
	GET URL: {{chargeStationsBaseUrl}}/api/v1/charge-stations/zip/{zipCode}
    GET URL sample: {{chargeStationsBaseUrl}}/api/v1/charge-stations/zip/00617
	Sample Response: 
	   [
           {
               "chargeStationId": "1767",
               "zipCode": "00617",
               "location": {
                   "x": -66.559696,
                   "y": 18.445147,
                   "type": "Point",
                   "coordinates": [
                       -66.559696,
                       18.445147
                   ]
               }
           },
           {
               "chargeStationId": "1768",
               "zipCode": "00617",
               "location": {
                   "x": -66.559695,
                   "y": 18.445145,
                   "type": "Point",
                   "coordinates": [
                       -66.559696,
                       18.445147
                   ]
               }
            },
       ]
#### getChargeStationsById
    GET URL: chargeStationsBaseUrl}}/api/v1/charge-stations/id/{id}
    GET URL sample: chargeStationsBaseUrl}}/api/v1/charge-stations/id/1767
    Sample Response:
      {
          "chargeStationId": "1767",
          "zipCode": "00617",
          "location": {
              "x": -66.559696,
              "y": 18.445147,
              "type": "Point",
              "coordinates": [
                  -66.559696,
                  18.445147
              ]
          }
      }
#### postChargingStations 
    POST URL: {{chargeStationsBaseUrl}}/api/v1/charge-stations
    Body: JSON
    Sample Request Body: 
       [
       {"zipCode" : "00601", "latitude" : "18.180555" , "longitude" :"-66.749961" },
       {"zipCode" : "00602", "latitude" : "18.361945" , "longitude" :"-67.175597" },
       {"zipCode" : "00603", "latitude" : "18.455183" , "longitude" :"-67.119887" },
       {"zipCode" : "00606", "latitude" : "18.158345" , "longitude" :"-66.932911" },
       {"zipCode" : "00610", "latitude" : "18.295366" , "longitude" :"-67.125135" },
       {"zipCode" : "00612", "latitude" : "18.402253" , "longitude" :"-66.711397" },
       {"zipCode" : "00616", "latitude" : "18.420412" , "longitude" :"-66.671979" }
       ]
       
    Sample Response: 
    [
        {
            "chargeStationId": "1298",
            "zipCode": "00601",
            "location": {
                "x": -66.749961,
                "y": 18.180555,
                "type": "Point",
                "coordinates": [
                    -66.749961,
                    18.180555
                ]
            }
        },
        {
            "chargeStationId": "1299",
            "zipCode": "00602",
            "location": {
                "x": -67.175597,
                "y": 18.361945,
                "type": "Point",
                "coordinates": [
                    -67.175597,
                    18.361945
                ]
            }
        },
        {
            "chargeStationId": "1300",
            "zipCode": "00603",
            "location": {
                "x": -67.119887,
                "y": 18.455183,
                "type": "Point",
                "coordinates": [
                    -67.119887,
                    18.455183
                ]
            }
        },
        {
            "chargeStationId": "1301",
            "zipCode": "00606",
            "location": {
                "x": -66.932911,
                "y": 18.158345,
                "type": "Point",
                "coordinates": [
                    -66.932911,
                    18.158345
                ]
            }
        },
        {
            "chargeStationId": "1302",
            "zipCode": "00610",
            "location": {
                "x": -67.125135,
                "y": 18.295366,
                "type": "Point",
                "coordinates": [
                    -67.125135,
                    18.295366
                ]
            }
        },
        {
            "chargeStationId": "1303",
            "zipCode": "00612",
            "location": {
                "x": -66.711397,
                "y": 18.402253,
                "type": "Point",
                "coordinates": [
                    -66.711397,
                    18.402253
                ]
            }
        }
     ]
       
	


